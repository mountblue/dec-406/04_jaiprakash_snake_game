/*
* snake_games  is a snake game script in which user can play the snake game.
*This script  providing the start and pause features to the game &shows the score details. 
*here canvas is area for drawing shapes on the screen.
*snake_length is varible which is default snake length.
*up,down,left,right is a direction variable which specify in which direction snake will move.
*/
var canvas;
var area;
var snake_length=5;
var timeId;
var left=0;
var right=0;
var up=0;
var down=0;
var snake=[];
var food;
var prev_direction=" "
var temp={x:0, y:0	};
var score=0;
var pauseClickCount=0;

/* setup() function adding the keydown listener and initiallize the canvas */
function setup() {
  clearInterval(timeId);
  clearData();
  document.addEventListener("keydown",direction);
  canvas=document.getElementById("canvas");
  area=canvas.getContext("2d");
  area.fillStyle="lightgrey";
  area.fillRect(0,0,canvas.width,canvas.height)
  start();
}

/* start function will create snake  & food and start the game */
function  start(){
  createSnake();
  createFood();
  timeId=setInterval(loop,100);
}

/*crateSnake() function will create the snake at default location*/
function createSnake() {
  snake[0]= { 
			  x:2*snake_length, 
			  y:5*snake_length	
			}
}

/*createFood() function will create the food for snake at any random position within the canvas*/
function createFood() {
  food={
	     x:Math.floor(Math.random()*60)*snake_length,
		 y:Math.floor(Math.random()*30)*snake_length
	   }
  area.fillStyle="black";
  area.fillRect(food.x,food.y,snake_length,snake_length);
}

/*loop is variable which points to the function that will does the actul part of the game i.e moving the snake , 
*when to create the food , adding the score and shows that score */
var loop=function() {
  var x_pos=canvas.width;
  var y_pos=canvas.height;
  for(let i=0;i<snake.length;i++)
  {
    area.clearRect(temp.x,temp.y,snake_length,snake_length);
    area.fillStyle="lightgrey";
	area.fillRect(temp.x,temp.y,snake_length,snake_length)
	area.fillStyle = ( i === 0 )? "green" : "red";
	area.fillRect(snake[i].x,snake[i].y,snake_length,snake_length);
   }		
  snakeX=snake[0].x;
  snakeY=snake[0].y;
  if(up===1) {	
    if(prev_direction!=="down"||snake.length===1) 
	{
	  snakeY-=snake_length;
	  prev_direction="up";
	}
	else
	  snakeY+=snake_length;
  }
  else if(down==1)
  {
    if(prev_direction!=="up"||snake.length===1) 
	{
	  snakeY+=snake_length;
	  prev_direction="down";
	}
	else
	  snakeY-=snake_length;
	}
	else if(left==1)
	{
	  if(prev_direction!=="right"||snake.length===1) 
	  {
	    snakeX-=snake_length;
		prev_direction="left";
	  }
	  else
	    snakeX+=snake_length;	
	}
	else if(right==1)
	{			
	  if(prev_direction!=="left"||snake.length===1) 
	  {
	    snakeX+=snake_length;
		prev_direction="right";
	  }
	  else
	    snakeX-=snake_length;
	}
	let newhead={
	              x:snakeX,
				  y:snakeY
				}
    if(newhead.x===-snake_length||newhead.x==canvas.width||newhead.y===-snake_length||newhead.y===canvas.height)
	  endGame();
	if(snake.length!==1)
	{
	  for(let i=0;i<snake.length;i++)
	  {
	    if(newhead.x===snake[i].x&&newhead.y===snake[i].y)
		  endGame();
	  }
	} 
	snake.unshift(newhead);
	if(food.x===snakeX&&food.y==snakeY)
	{
	  score+=5;
	  document.getElementById("score").innerHTML="Score :  "+score;
	  createFood();
	}
	else
	  temp=snake.pop();
};
/*endGame() function will end the game*/	
function endGame(){
  clearInterval(timeId);
}
		
/* direction()  function will set the direction to which snake has to move when user press any up down left right key from the keyboard*/
function direction(event) {
  if (event.key==="ArrowUp")
  {	
    up=1;down=0;left=0;right=0;
  }
  if (event.key==="ArrowDown")
  {
    down=1;up=0;left=0;right=0;
  }
  if (event.key==="ArrowLeft")
  {	
      left=1;up=0;down=0;right=0;
  }	
  if (event.key==="ArrowRight")
  {
    right=1;up=0;down=0;left=0;
  }			
}

/* Pause() function will pause the game till the user doesn't press the pause button again*/
function pause() {
  if(pauseClickCount%2==0)
  {
    clearInterval(timeId);
  }
  else
    timeId=setInterval(loop,100);
	pauseClickCount++;
}

/*clearDate() function will clear all the variables and set them their default values*/
function clearData() {
 canvas=null;
 area=null;
 snake_length=5;
 timeId=null;
 left=0;
 right=0;
 up=0;
 down=0;
 snake=[];
 food=null;
 prev_direction=" "
 temp={x:0, y:0	};
 score=0;
 pauseClickCount=0;
}